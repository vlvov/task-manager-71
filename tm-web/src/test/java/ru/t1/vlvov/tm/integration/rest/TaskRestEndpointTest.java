package ru.t1.vlvov.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.vlvov.tm.dto.Result;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.model.Task;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    private static String sessionId;

    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private final Task task1 = new Task("Task1");

    @NotNull
    private final Task task2 = new Task("Task2");

    @NotNull
    private final Task task3 = new Task("Task3");

    @NotNull
    private final Task task4 = new Task("Task4");

    @NotNull
    private static final HttpHeaders headers= new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                "http://localhost:8080/api/auth/getlogin?username=test&password=test";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        @NotNull HttpHeaders headersResponse = response.getHeaders();
        List<java.net.HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        headers.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    )
    {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<Task> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    )
    {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Task.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity(task1, headers));
        sendRequest(url, HttpMethod.POST, new HttpEntity(task3, headers));
    }

    @After
    public void clean() {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(headers));
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(headers));
    }

    @Test
    public void addTest() {
        @NotNull final String url = BASE_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity(task2, headers));
        @NotNull final String findEntityUrl = BASE_URL + "findById/" + task2.getId();
        Assert.assertNotNull(sendRequest(findEntityUrl, HttpMethod.GET, new HttpEntity(task1, headers)));
    }

    @Test
    public void findAllTest() {
        @NotNull final String findAllUrl = BASE_URL + "findAll/";
        Assert.assertTrue(sendRequestList(findAllUrl, HttpMethod.GET, new HttpEntity(task1, headers)).getBody().size() > 0);
    }

    @Test
    public void removeTest() {
        @NotNull final String url = BASE_URL + "deleteById/" + task1.getId();
        sendRequest(url, HttpMethod.POST, new HttpEntity(task1, headers));
        @NotNull final String findEntityUrl = BASE_URL + "findById/" + task2.getId();
        Assert.assertEquals(200, sendRequest(findEntityUrl, HttpMethod.GET, new HttpEntity(task1, headers)).getStatusCodeValue());
    }

}
