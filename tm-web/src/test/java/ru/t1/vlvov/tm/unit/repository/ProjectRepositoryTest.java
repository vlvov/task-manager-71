package ru.t1.vlvov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.config.ApplicationConfiguration;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    private final Project project1 = new Project();

    private final Project project2 = new Project();

    private final Project project3 = new Project();

    private final Project project4 = new Project();

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token
                = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectRepository.deleteByUserId(UserUtil.getUserId());
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
        projectRepository.save(project3);
        projectRepository.save(project4);
    }

    @After
    public void clean() {
        projectRepository.deleteByUserId(UserUtil.getUserId());
    }

    @Test
    public void findFirstByUserIdAndIdTest() {
        final Project projectTest = projectRepository.findFirstByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project1.getId(), projectTest.getId());
    }

    @Test
    public void findByUserIdTest(){
        Assert.assertEquals(3, projectRepository.findByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByUserIdAndIdTest() {
        Project projectTest = projectRepository.findFirstByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project1.getId(), projectTest.getId());
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), projectTest.getId());
        projectTest = projectRepository.findFirstByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertNull(projectTest);
    }

    @Test
    public void deleteByUserId() {
        Assert.assertEquals(3, projectRepository.findByUserId(UserUtil.getUserId()).size());
        projectRepository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectRepository.findByUserId(UserUtil.getUserId()).size());
    }

}
