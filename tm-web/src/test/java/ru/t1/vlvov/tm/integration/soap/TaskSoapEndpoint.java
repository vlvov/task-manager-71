package ru.t1.vlvov.tm.integration.soap;

import com.google.common.net.HttpHeaders;
import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.IAuthEndpoint;
import ru.t1.vlvov.tm.api.ITaskEndpoint;
import ru.t1.vlvov.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.vlvov.tm.client.soap.TaskSoapEndpointClient;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.model.Task;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(IntegrationCategory.class)
public class TaskSoapEndpoint {


    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static ITaskEndpoint taskEndpoint;

    private final static String BASE_URL = "http://localhost:8080";

    @NotNull
    private final Task task1 = new Task("Task1");

    @NotNull
    private final Task task2 = new Task("Task2");

    @NotNull
    private final Task task3 = new Task("Task3");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(BASE_URL);
        Assert.assertTrue(authEndpoint.login("test", "test").isSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(BASE_URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void init() {
        taskEndpoint.save(task1);
        taskEndpoint.save(task2);
    }

    @After
    public void clean() {
        taskEndpoint.deleteAll();
    }

    @Test
    public void addTest() {
        taskEndpoint.save(task3);
        Assert.assertNotNull(taskEndpoint.findById(task3.getId()));
    }

    @Test
    public void deleteByIdTest() {
        taskEndpoint.deleteById(task1.getId());
        Assert.assertNull(taskEndpoint.findById(task1.getId()));
    }

    @Test
    public void findByIdTest() {
        Assert.assertNotNull(taskEndpoint.findById(task2.getId()));
    }


}
