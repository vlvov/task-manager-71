package ru.t1.vlvov.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.vlvov.tm.dto.Result;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.model.Project;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    private static String sessionId;

    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final Project project1 = new Project("Project1");

    @NotNull
    private final Project project2 = new Project("Project2");

    @NotNull
    private final Project project3 = new Project("Project3");

    @NotNull
    private final Project project4 = new Project("Project4");

    @NotNull
    private static final HttpHeaders headers= new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                "http://localhost:8080/api/auth/getlogin?username=test&password=test";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        @NotNull HttpHeaders headersResponse = response.getHeaders();
        List<java.net.HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        headers.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    private static ResponseEntity<List> sendRequestList(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
            )
    {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<Project> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    )
    {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Project.class);
    }

    @Before
    public void initTest() {
        @NotNull final String url = BASE_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity(project1, headers));
        sendRequest(url, HttpMethod.POST, new HttpEntity(project3, headers));
    }

    @After
    public void clean() {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(headers));
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(headers));
    }

    @Test
    public void addTest() {
        @NotNull final String url = BASE_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity(project2, headers));
        @NotNull final String findEntityUrl = BASE_URL + "findById/" + project2.getId();
        Assert.assertNotNull(sendRequest(findEntityUrl, HttpMethod.GET, new HttpEntity(project1, headers)));
    }

    @Test
    public void findAllTest() {
        @NotNull final String findAllUrl = BASE_URL + "findAll/";
        Assert.assertTrue(sendRequestList(findAllUrl, HttpMethod.GET, new HttpEntity(project1, headers)).getBody().size() > 0);
    }

    @Test
    public void removeTest() {
        @NotNull final String url = BASE_URL + "deleteById/" + project1.getId();
        sendRequest(url, HttpMethod.POST, new HttpEntity(project1, headers));
        @NotNull final String findEntityUrl = BASE_URL + "findById/" + project2.getId();
        Assert.assertEquals(200, sendRequest(findEntityUrl, HttpMethod.GET, new HttpEntity(project1, headers)).getStatusCodeValue());
    }

}
