package ru.t1.vlvov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.config.ApplicationConfiguration;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.repository.TaskRepository;
import ru.t1.vlvov.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    private final Task task1 = new Task();

    private final Task task2 = new Task();

    private final Task task3 = new Task();

    private final Task task4 = new Task();

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token
                = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskRepository.deleteByUserId(UserUtil.getUserId());
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
        taskRepository.save(task3);
        taskRepository.save(task4);
    }

    @After
    public void clean() {
        taskRepository.deleteByUserId(UserUtil.getUserId());
    }

    @Test
    public void findFirstByUserIdAndIdTest() {
        final Task taskTest = taskRepository.findFirstByUserIdAndId(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task1.getId(), taskTest.getId());
    }

    @Test
    public void findByUserIdTest(){
        Assert.assertEquals(3, taskRepository.findByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByUserIdAndIdTest() {
        Task taskTest = taskRepository.findFirstByUserIdAndId(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task1.getId(), taskTest.getId());
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), taskTest.getId());
        taskTest = taskRepository.findFirstByUserIdAndId(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskTest);
    }

    @Test
    public void deleteByUserId() {
        Assert.assertEquals(3, taskRepository.findByUserId(UserUtil.getUserId()).size());
        taskRepository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskRepository.findByUserId(UserUtil.getUserId()).size());
    }

}
