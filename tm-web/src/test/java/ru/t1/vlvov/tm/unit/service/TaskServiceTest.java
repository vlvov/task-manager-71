package ru.t1.vlvov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.config.ApplicationConfiguration;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.repository.TaskRepository;
import ru.t1.vlvov.tm.service.TaskService;
import ru.t1.vlvov.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    private final Task task1 = new Task();

    private final Task task2 = new Task();

    private final Task task3 = new Task();

    private final Task task4 = new Task();

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token
                = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.removeAll(UserUtil.getUserId());
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        taskService.add(UserUtil.getUserId(), task1);
        taskService.add(UserUtil.getUserId(), task2);
        taskService.add(UserUtil.getUserId(), task3);
        taskService.add(UserUtil.getUserId(), task4);
    }

    @After
    public void clean() {
        taskService.removeAll(UserUtil.getUserId());
    }

    @Test
    public void clearTest() {
        Assert.assertTrue(taskService.findAll(UserUtil.getUserId()).size() > 0);
        taskService.clear(UserUtil.getUserId());
        Assert.assertEquals(0, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void addTest() {
        taskService.remove(UserUtil.getUserId(), task1);
        Task taskTest = taskService.findOneById(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskTest);
        taskService.add(UserUtil.getUserId(), task1);
        taskTest = taskService.findOneById(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task1.getId(), taskTest.getId());
    }

    @Test
    public void findAllTest(){
        Assert.assertTrue(taskService.findAll(UserUtil.getUserId()).size() > 0);
    }

    @Test
    public void removeTest() {
        Task taskTest = taskService.findOneById(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task1.getId(), taskTest.getId());
        taskService.remove(UserUtil.getUserId(), task1);
        taskTest = taskService.findOneById(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskTest);
    }

    @Test
    public void removeByIdTest() {
        Task taskTest = taskService.findOneById(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task1.getId(), taskTest.getId());
        taskService.removeById(UserUtil.getUserId(), task1.getId());
        taskTest = taskService.findOneById(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskTest);
    }

    @Test
    public void findOneByIdTest() {
        final Task taskTest = taskService.findOneById(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task1.getId(), taskTest.getId());
    }


}
