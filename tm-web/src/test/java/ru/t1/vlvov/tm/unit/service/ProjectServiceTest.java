package ru.t1.vlvov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.config.ApplicationConfiguration;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.service.ProjectService;
import ru.t1.vlvov.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    private final Project project1 = new Project();

    private final Project project2 = new Project();

    private final Project project3 = new Project();

    private final Project project4 = new Project();

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token
                = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectService.removeAll(UserUtil.getUserId());
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        projectService.add(UserUtil.getUserId(), project1);
        projectService.add(UserUtil.getUserId(), project2);
        projectService.add(UserUtil.getUserId(), project3);
        projectService.add(UserUtil.getUserId(), project4);
    }

    @After
    public void clean() {
        projectService.removeAll(UserUtil.getUserId());
    }

    @Test
    public void clearTest() {
        Assert.assertTrue(projectService.findAll(UserUtil.getUserId()).size() > 0);
        projectService.clear(UserUtil.getUserId());
        Assert.assertEquals(0, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void addTest() {
        projectService.remove(UserUtil.getUserId(), project1);
        Project projectTest = projectService.findOneById(UserUtil.getUserId(), project1.getId());
        Assert.assertNull(projectTest);
        projectService.add(UserUtil.getUserId(), project1);
        projectTest = projectService.findOneById(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project1.getId(), projectTest.getId());
    }

    @Test
    public void findAllTest(){
        Assert.assertTrue(projectService.findAll(UserUtil.getUserId()).size() > 0);
    }

    @Test
    public void removeTest() {
        Project projectTest = projectService.findOneById(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project1.getId(), projectTest.getId());
        projectService.remove(UserUtil.getUserId(), project1);
        projectTest = projectService.findOneById(UserUtil.getUserId(), project1.getId());
        Assert.assertNull(projectTest);
    }

    @Test
    public void removeByIdTest() {
        Project projectTest = projectService.findOneById(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project1.getId(), projectTest.getId());
        projectService.removeById(UserUtil.getUserId(), project1.getId());
        projectTest = projectService.findOneById(UserUtil.getUserId(), project1.getId());
        Assert.assertNull(projectTest);
    }

    @Test
    public void findOneByIdTest() {
        final Project projectTest = projectService.findOneById(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project1.getId(), projectTest.getId());
    }

}
