package ru.t1.vlvov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.repository.TaskRepository;
import ru.t1.vlvov.tm.service.ProjectService;
import ru.t1.vlvov.tm.service.TaskService;
import ru.t1.vlvov.tm.util.UserUtil;

import java.util.Collection;

@Controller
public class TasksController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @ModelAttribute("projects")
    public Collection<Project> getProjects() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("task-list", "tasks", taskService.findAll(UserUtil.getUserId()));
    }

}
