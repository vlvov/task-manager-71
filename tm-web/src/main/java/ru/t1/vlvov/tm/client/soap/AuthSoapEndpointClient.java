package ru.t1.vlvov.tm.client.soap;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.api.IAuthEndpoint;
import ru.t1.vlvov.tm.endpoint.AuthEndpoint;
import ru.t1.vlvov.tm.endpoint.ProjectEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class AuthSoapEndpointClient {

    public static IAuthEndpoint getInstance(@NotNull final String baseUrl) throws MalformedURLException {
        @NotNull final String wsdl = baseUrl + "/ws/AuthEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "AuthEndpointService";
        @NotNull final String ns = "http://endpoint.tm.vlvov.t1.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IAuthEndpoint result = Service.create(url, name).getPort(IAuthEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
