package ru.t1.vlvov.tm.util;

import lombok.SneakyThrows;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.vlvov.tm.dto.CustomUser;

import java.nio.file.AccessDeniedException;

public final class UserUtil {

    private UserUtil() {
    }

    @SneakyThrows
    public static String getUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Object principal = authentication.getPrincipal();
        if (principal == null) throw new AccessDeniedException("");
        if (!(principal instanceof CustomUser)) throw new AccessDeniedException("");
        final CustomUser user = (CustomUser) principal;
        return user.getUserId();
    }

}
