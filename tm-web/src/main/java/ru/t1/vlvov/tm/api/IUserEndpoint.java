package ru.t1.vlvov.tm.api;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.vlvov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.Collection;

public interface IUserEndpoint {

    @WebMethod
    @PostMapping("/save")
    User save(
            @WebParam(name = "user", partName = "user")
            @RequestBody User user
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "user", partName = "user")
            @RequestBody User user
    );

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/findAll")
    Collection<User> findAll();

    @WebMethod
    @PostMapping("/findById/{id}")
    User findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

}
