package ru.t1.vlvov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.service.ProjectService;
import ru.t1.vlvov.tm.util.UserUtil;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectService.findAll(UserUtil.getUserId()));
    }

}
