package ru.t1.vlvov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataBase64LoadRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class DataBase64LoadListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Load data from base 64 file.";

    @NotNull
    private final String NAME = "data-load-text";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBase64LoadListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA LOAD BASE64]");
        @NotNull DataBase64LoadRequest request = new DataBase64LoadRequest(getToken());
        domainEndpoint.loadDataBase64(request);
    }

}
