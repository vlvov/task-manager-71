package ru.t1.vlvov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.dto.model.UserDTO;

import java.util.Arrays;
import java.util.List;

public final class UserTestData {

    @NotNull
    public final static UserDTO USER1 = new UserDTO("USER1");

    @NotNull
    public final static UserDTO USER2 = new UserDTO("USER2");

    @NotNull
    public final static UserDTO USER3 = new UserDTO("USER3");

    @NotNull
    public final static UserDTO ADMIN1 = new UserDTO("ADMIN1");

    @NotNull
    public final static String testLogin = "test_login";

    @NotNull
    public final static String testEmail = "test_email";

    @NotNull
    public final static String testPassword = "test_password";

    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(USER1, USER2, ADMIN1);

    @NotNull
    public final static List<UserDTO> USER_LIST2 = Arrays.asList(USER3, ADMIN1);

}
