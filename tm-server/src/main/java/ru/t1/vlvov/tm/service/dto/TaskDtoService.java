package ru.t1.vlvov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.service.dto.ITaskDtoService;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.enumerated.CustomSort;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.exception.field.NameEmptyException;
import ru.t1.vlvov.tm.repository.dto.TaskDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO> implements ITaskDtoService {

    @Autowired
    @NotNull
    private TaskDtoRepository taskRepositoryDTO;

    @Override
    @NotNull
    @Transactional
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO model = new TaskDTO();
        model.setName(name);
        model.setUserId(userId);
        taskRepositoryDTO.save(model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO create(@Nullable final String userId, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO model = new TaskDTO();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        taskRepositoryDTO.save(model);
        return model;
    }

    @Override
    @Nullable
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        return taskRepositoryDTO.findAllByProjectId(projectId);
    }

    @Override
    @Nullable
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return taskRepositoryDTO.findAllByProjectId(projectId);
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        taskRepositoryDTO.save(model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        taskRepositoryDTO.save(model);
        return model;
    }

    @Override
    @Transactional
    public void clear() {
        taskRepositoryDTO.deleteAll();
    }

    @Override
    @Transactional
    public void add(@Nullable TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<TaskDTO> collection) {
        clear();
        taskRepositoryDTO.saveAll(collection);
    }

    @Override
    @Nullable
    public List<TaskDTO> findAll() {
        return taskRepositoryDTO.findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepositoryDTO.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        taskRepositoryDTO.deleteById(id);
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepositoryDTO.existsById(id);
    }

    @Override
    public @Nullable TaskDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepositoryDTO.findFirstById(id);
    }

    @Override
    @Transactional
    public void add(@Nullable String userId, @Nullable TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        model.setUserId(userId);
        taskRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable String userId, @Nullable TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        model.setUserId(userId);
        taskRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        taskRepositoryDTO.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public List<TaskDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return taskRepositoryDTO.findAllByUserId(userId);
    }

    @Override
    public @Nullable TaskDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return taskRepositoryDTO.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable String userId, @Nullable TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        taskRepositoryDTO.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        TaskDTO model = findOneById(userId, id);
        remove(userId, model);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<TaskDTO> findAll(@Nullable String userId, @Nullable CustomSort sort) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (sort == null) return findAll(userId);
        final Sort sortable = Sort.by(Sort.Direction.ASC, CustomSort.getOrderByField(sort));
        return taskRepositoryDTO.findAllByUserId(userId, sortable);
    }

}
