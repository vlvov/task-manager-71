package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> {

    @Nullable
    TaskDTO findFirstById(@NotNull final String id);

    @Nullable
    List<TaskDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    TaskDTO findFirstByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @Nullable
    List<TaskDTO> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @Nullable
    List<TaskDTO> findAllByProjectId(@NotNull final String projectId);

    @Nullable
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    void deleteAllByUserId(@NotNull final String userId);

}
