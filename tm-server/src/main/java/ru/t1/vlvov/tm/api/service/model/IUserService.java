package ru.t1.vlvov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.User;

public interface IUserService extends IService<User> {

    @Nullable
    User create(@Nullable final String login, @Nullable final String password);

    @Nullable
    User create(@Nullable final String login, @Nullable final String password, @Nullable final String email);

    @Nullable
    User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role);

    @Nullable
    User findByLogin(@Nullable final String login);

    @Nullable
    User findByEmail(@Nullable final String email);

    @NotNull
    User removeByLogin(@Nullable final String login);

    @NotNull
    User setPassword(@Nullable final String id, @Nullable final String password);

    @NotNull
    User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    );

    boolean isLoginExist(@Nullable final String login);

    boolean isEmailExist(@Nullable final String email);

    @NotNull
    User lockUserByLogin(@Nullable final String id);

    @NotNull
    User unlockUserByLogin(@Nullable final String id);

}
