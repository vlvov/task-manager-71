package ru.t1.vlvov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void log(@NotNull final String fileName, @NotNull final String text);

}
